using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Rendering;
using Unity.Transforms;
using Random = UnityEngine.Random;

public class PlantSpawn : MonoBehaviour
{
    [SerializeField] private int numberToSpawn;
    
    public GameObject plantPrefab;
    public GameObject plantHead;
    private List<Entity> _plants = new List<Entity>();
    private BlobAssetStore _blobAssetStore;
    
    private void Awake()
    {
        _blobAssetStore = new BlobAssetStore();
        var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, _blobAssetStore);
        var prefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(plantPrefab, settings);
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        
        while (_plants.Count < numberToSpawn)
        {
            var x = Random.Range(0, 300);
            var z = Random.Range(0, 300);
            Vector3 pos = new Vector3(x, 0, z);
            
            var currentEntity = entityManager.Instantiate(prefab);
            entityManager.AddComponentData(currentEntity, new Translation {Value = pos});
            entityManager.AddComponentData(currentEntity, new LocalToWorld());

            var head = plantPrefab.transform.GetChild(5).gameObject;
            var headPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(head, settings);
            var childEntity = entityManager.Instantiate(headPrefab);
            
            entityManager.AddComponentData(childEntity, new Parent {Value = currentEntity});
            entityManager.AddComponentData(childEntity, new Scale {Value = plantHead.transform.localScale.x});
            entityManager.AddComponentData(childEntity, new LocalToWorld());
            entityManager.AddComponentData(childEntity, new LocalToParent());
            entityManager.AddSharedComponentData(childEntity, new RenderMesh {mesh = plantHead.GetComponent<MeshCollider>().sharedMesh, material = plantHead.GetComponent<MeshRenderer>().sharedMaterial});
            
            var data = entityManager.GetComponentData<FallschirmData>(currentEntity);
            data.head = childEntity;
            entityManager.SetComponentData(currentEntity, data);
            
            _plants.Add(currentEntity);
        }
    }

    private void OnDestroy()
    {
        if (_blobAssetStore != null)
        {
            _blobAssetStore.Dispose();
        }
    }
}
