using Unity.Entities;

[GenerateAuthoringComponent]
public struct WindInfluenceableData : IComponentData
{
    public float Resistance;
    public bool IsAttached;
}

