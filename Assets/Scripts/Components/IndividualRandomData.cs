﻿using Unity.Entities;
using Random = Unity.Mathematics.Random;

namespace Components
{
    [GenerateAuthoringComponent]
    public struct IndividualRandomData : IComponentData
    {
        public Random Value;
    }
}