using Unity.Entities;

[GenerateAuthoringComponent]
public struct FallschirmData : IComponentData
{
    public Entity fallschirmPrefab;
    public int numberToSpawn;
    public Entity head;
}
