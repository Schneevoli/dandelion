﻿using System;
using Unity.Entities;
using UnityEngine;

namespace Components
{
    [Serializable]
    [GenerateAuthoringComponent]
    public struct Fly : IComponentData
    {
        public Quaternion targetAlignment;
        public float yRotationPerSecond;
    }
}
