using Unity.Entities;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;
using Random = UnityEngine.Random;

public class FallschirmSpawnSystem : ComponentSystem
{
    protected override void OnUpdate()
    {
        
    }

    protected override void OnStartRunning()
    {
        var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var spawnedEntities = 0;

        Entities.WithAll<FallschirmData>().ForEach((Entity entity, ref FallschirmData fallschirmData) =>
        {
            var renderMesh = EntityManager.GetSharedComponentData<RenderMesh>(fallschirmData.head);
            var scale = EntityManager.GetComponentData<Scale>(fallschirmData.head);
            var childTranslation = EntityManager.GetComponentData<Translation>(fallschirmData.head);
            var parentTranslation = EntityManager.GetComponentData<Translation>(entity);
            var actualPos = childTranslation.Value+ parentTranslation.Value;
            
            while (spawnedEntities < fallschirmData.numberToSpawn)
            {
                Vector3 randomPoint = GetRandomPointOnMesh(renderMesh.mesh);
                randomPoint = Vector3.Scale(randomPoint, new Vector3(scale.Value, scale.Value, scale.Value));
                randomPoint += (Vector3) actualPos;

                var currentEntity = entityManager.Instantiate(fallschirmData.fallschirmPrefab);
                
                entityManager.AddComponentData(currentEntity, new LocalToWorld());
                entityManager.AddComponentData(currentEntity, new Translation {Value = randomPoint});
                entityManager.AddComponentData(currentEntity,
                    new Rotation {Value = Quaternion.FromToRotation(Vector3.up, randomPoint - (Vector3) actualPos)});

                var windInfluenceable = entityManager.GetComponentData<WindInfluenceableData>(currentEntity);
                windInfluenceable.IsAttached = true;
                windInfluenceable.Resistance = Random.Range(PlayerInput.MinInputLevel, PlayerInput.MaxInputLevel);
                entityManager.SetComponentData(currentEntity, windInfluenceable);

                Translation trans = entityManager.GetComponentData<Translation>(currentEntity);

                if (trans.Value.y < 60)
                {
                    entityManager.DestroyEntity(currentEntity);
                }
                else
                {
                    spawnedEntities++;
                }
            }
            spawnedEntities = 0;
        });
    }
    
    private Vector3 GetRandomPointOnMesh(Mesh mesh)
    {
        //if you're repeatedly doing this on a single mesh, you'll likely want to cache cumulativeSizes and total
        float[] sizes = GetTriSizes(mesh.triangles, mesh.vertices);
        float[] cumulativeSizes = new float[sizes.Length];
        float total = 0;

        for (int i = 0; i < sizes.Length; i++)
        {
            total += sizes[i];
            cumulativeSizes[i] = total;
        }

        //so everything above this point wants to be factored out

        float randomsample = Random.value* total;

        int triIndex = -1;
        
        for (int i = 0; i < sizes.Length; i++)
        {
            if (randomsample <= cumulativeSizes[i])
            {
                triIndex = i;
                break;
            }
        }

        if (triIndex == -1) Debug.LogError("triIndex should never be -1");

        Vector3 a = mesh.vertices[mesh.triangles[triIndex * 3]];
        Vector3 b = mesh.vertices[mesh.triangles[triIndex * 3 + 1]];
        Vector3 c = mesh.vertices[mesh.triangles[triIndex * 3 + 2]];

        //generate random barycentric coordinates

        float r = Random.value;
        float s = Random.value;

        if(r + s >=1)
        {
            r = 1 - r;
            s = 1 - s;
        }
        //and then turn them back to a Vector3
        Vector3 pointOnMesh = a + r*(b - a) + s*(c - a);
        return pointOnMesh;

    }

    private float[] GetTriSizes(int[] tris, Vector3[] verts)
    {
        int triCount = tris.Length / 3;
        float[] sizes = new float[triCount];
        for (int i = 0; i < triCount; i++)
        {
            sizes[i] = .5f * Vector3.Cross(verts[tris[i * 3 + 1]] - verts[tris[i * 3]],
                verts[tris[i * 3 + 2]] - verts[tris[i * 3]]).magnitude;
        }

        return sizes;
    }
}
