using Components;
using Unity.Entities;
using Unity.Physics;
using Unity.Physics.Extensions;
using Unity.Transforms;
using UnityEngine;

public class WindSystem : SystemBase
{
    private PlayerInput _playerInput;
    private Camera _mainCamera;
    
    protected override void OnStartRunning()
    {
        _playerInput = Object.FindObjectOfType<PlayerInput>();
        _mainCamera = Camera.main;
        Entities.WithAll<IndividualRandomData>().ForEach(
            (Entity e, int entityInQueryIndex, ref IndividualRandomData randomData) =>
            {
                randomData.Value = Unity.Mathematics.Random.CreateFromIndex((uint)entityInQueryIndex);
            }).ScheduleParallel();
    }

    protected override void OnUpdate()
    {
        if (_playerInput.InputLevel < PlayerInput.MinInputLevel) return;
        var deltaTime = Time.DeltaTime;
        var InputLevel = _playerInput.InputLevel;
        var _mainCameraPos = _mainCamera.transform.position;

        Entities.WithAll<WindInfluenceableData>().ForEach(
            (Entity entity, ref WindInfluenceableData windInfluenceableData, ref PhysicsVelocity physicsVelocity,
                ref PhysicsMass physicsMass, ref PhysicsGravityFactor physicsGravityFactor, ref IndividualRandomData randomData, ref Translation translation) =>
            {
                if (windInfluenceableData.IsAttached && windInfluenceableData.Resistance > InputLevel) return;

                if (windInfluenceableData.IsAttached)
                {
                    windInfluenceableData.IsAttached = false;
                    physicsGravityFactor.Value = 0.025f;
                }

                var windForce = ((Vector3)translation.Value - _mainCameraPos).normalized * 10f * deltaTime * InputLevel;
                physicsVelocity.ApplyLinearImpulse(physicsMass, windForce);

                var x = randomData.Value.NextFloat(-4, 5);
                var y = randomData.Value.NextFloat(-4, 5);
                var z = randomData.Value.NextFloat(-4, 5);

                var randomForce = new Vector3(x, y, z) * 3f * deltaTime;
                physicsVelocity.ApplyLinearImpulse(physicsMass,randomForce);
            })
        .ScheduleParallel();
    }
}


