﻿using Components;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace Systems
{
    internal class FlySystem : SystemBase
    {
        private const float AlignmentLerpDurationSeconds = 5f;

        private struct InitFlyJob : IJobEntityBatch
        {
            public ComponentTypeHandle<Fly> FlyTypeHandle;
            [ReadOnly] public ComponentTypeHandle<IndividualRandomData> RandomTypeHandle;

            [BurstCompile]
            public void Execute(ArchetypeChunk batchInChunk, int batchIndex)
            {
                var flyComponents = batchInChunk.GetNativeArray(FlyTypeHandle);
                var randomComponents = batchInChunk.GetNativeArray(RandomTypeHandle);
                for (var i = 0; i < batchInChunk.Count; i++)
                {
                    var fly = flyComponents[i];
                    var random = randomComponents[i];
                    var targetAlignmentX = random.Value.NextFloat(-30, 30);
                    var targetAlignmentY = random.Value.NextFloat(-30, 30);
                    fly.targetAlignment = Quaternion.Euler(targetAlignmentX, targetAlignmentY, 0);
                    fly.yRotationPerSecond = random.Value.NextFloat(-180, 180);
                    flyComponents[i] = fly;
                }
            }
        }

        private struct FlyJob : IJobEntityBatch
        {
            [ReadOnly] public ComponentTypeHandle<Fly> FlyTypeHandle;
            [ReadOnly] public ComponentTypeHandle<WindInfluenceableData> WindInfluenceHandle;
            public ComponentTypeHandle<Rotation> RotationTypeHandle;
            public float DeltaTime;

            [BurstCompile]
            public void Execute(ArchetypeChunk batchInChunk, int batchIndex)
            {
                var flyComponents = batchInChunk.GetNativeArray(FlyTypeHandle);
                var windInfluenceComponents = batchInChunk.GetNativeArray(WindInfluenceHandle);
                var rotationComponents = batchInChunk.GetNativeArray(RotationTypeHandle);
                for (var i = 0; i < batchInChunk.Count; i++)
                {
                    var fly = flyComponents[i];
                    var windInfluence = windInfluenceComponents[i];
                    var rotation = rotationComponents[i];
                    if (windInfluence.IsAttached) continue;
                    var alignmentRotation = rotation.Value * Quaternion.identity;
                    var yRotation = alignmentRotation * Quaternion.Euler(0, fly.yRotationPerSecond * DeltaTime, 0) * Quaternion.Inverse(alignmentRotation);
                    var alignmentLerpRotation = Quaternion.Lerp(rotation.Value, fly.targetAlignment, DeltaTime / AlignmentLerpDurationSeconds);
                    rotation.Value = yRotation * alignmentLerpRotation;
                    rotationComponents[i] = rotation;
                }
            }
        }

        private EntityQuery _entityQuery;

        protected override void OnCreate()
        {
            _entityQuery = GetEntityQuery(ComponentType.ReadOnly<Fly>(), ComponentType.ReadWrite<Rotation>());
        }

        protected override void OnStartRunning()
        {
            var initFlyJob = new InitFlyJob
            {
                FlyTypeHandle = GetComponentTypeHandle<Fly>(),
                RandomTypeHandle = GetComponentTypeHandle<IndividualRandomData>(true)
            };
            initFlyJob.Run(_entityQuery);
        }

        protected override void OnUpdate()
        {
            var flyJob = new FlyJob
            {
                FlyTypeHandle = GetComponentTypeHandle<Fly>(true),
                WindInfluenceHandle = GetComponentTypeHandle<WindInfluenceableData>(true),
                RotationTypeHandle = GetComponentTypeHandle<Rotation>(),
                DeltaTime = Time.DeltaTime
            };
            Dependency = flyJob.Schedule(_entityQuery, Dependency);
        }
    }
}
