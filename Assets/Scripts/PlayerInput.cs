using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private List<string> microphoneNames = new List<string>();
    [SerializeField] private Transform _playerTransform;
    [SerializeField] private float navigationSpeed = 2.4f;
    [SerializeField] private float shiftMultiplier = 2f;
    [SerializeField] private float sensitivity = 1.0f;
    public float InputLevel { get; private set; }
    public static readonly float MinInputLevel = 0.1f;
    public static readonly float MaxInputLevel = 0.5f;
 
    private Camera cam;
    private Vector3 anchorPoint;
    private Quaternion anchorRot;
    private const int _dec = 128;
    private float _updateStep = 0.2f;
    private float currentUpdateTime;

    private void Start()
    {
        cam = Camera.main;
        Microphone.devices.ToList().ForEach(device => microphoneNames.Add(device.ToString())); // Debug
        if (Microphone.devices.Length > 0)
        {
            _audioSource.Stop();
            _audioSource.clip = Microphone.Start(Microphone.devices[0].ToString(), true, 4, AudioSettings.outputSampleRate);
            _audioSource.loop = true;
            _audioSource.Play();
        }
    }

    public void Update()
    {
        AnalyzeSound();
        
        if(Input.GetMouseButton(1)) {
            Vector3 move = Vector3.zero;
            float speed = navigationSpeed * (Input.GetKey(KeyCode.LeftShift) ? shiftMultiplier : 1f) * Time.deltaTime * 9.1f;
            if(Input.GetKey(KeyCode.W))
                move += Vector3.forward * speed;
            if(Input.GetKey(KeyCode.S))
                move -= Vector3.forward * speed;
            if(Input.GetKey(KeyCode.D))
                move += Vector3.right * speed;
            if(Input.GetKey(KeyCode.A))
                move -= Vector3.right * speed;
            if(Input.GetKey(KeyCode.E))
                move += Vector3.up * speed;
            if(Input.GetKey(KeyCode.Q))
                move -= Vector3.up * speed;
            _playerTransform.Translate(move);
        }
 
        if(Input.GetMouseButtonDown(1)) {
            anchorPoint = new Vector3(Input.mousePosition.y, -Input.mousePosition.x);
            anchorRot = _playerTransform.rotation;
        }
        if(Input.GetMouseButton(1)) {
            Quaternion rot = anchorRot;
 
            Vector3 dif = anchorPoint - new Vector3(Input.mousePosition.y, -Input.mousePosition.x);
            rot.eulerAngles += dif * sensitivity;
            _playerTransform.rotation = rot;
        }
    }
    
    private void AnalyzeSound()
    {
        currentUpdateTime += Time.deltaTime;
        if (currentUpdateTime < _updateStep) return;
        
        currentUpdateTime = 0f;

        var clip = _audioSource.clip;
        var size = clip.samples * clip.channels;
        var waveData = new float[size];
        
        //get mic volume
        var micPosition = Microphone.GetPosition(null)-(_dec+1); // null means the first microphone
        _audioSource.clip.GetData(waveData, micPosition);
	       
        // Getting a peak on the last 128 samples
        float levelMax = 0;
        for (var i = 0; i < _dec; i++) {
            var wavePeak = waveData[i] * waveData[i];
            if (levelMax < wavePeak) {
                levelMax = wavePeak;
            }
        }
        InputLevel = Mathf.Sqrt(Mathf.Sqrt(levelMax));
    }
}
